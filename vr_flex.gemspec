# Replace extension with the name of your extension's .rb file
require './lib/vr_flex'

Gem::Specification.new do |s|
  # Release Specific Information
  #  Replace Extension with the name you used in your extension.rb
  #   in the mdodule with version and date.
  s.version = "1.0"
  s.date = "2015-04-03"

  # Gem Details
  # Replace "extension" with the name of your extension
  s.name = "vr_flex"
  s.rubyforge_project = "vr_flex"
  # Description of your extension
  s.description = %q{This extension implements all necessary syntaxes for today's use:
                    by one writing we get all syntaxes for successful work of flex-box.
                    It ensures that flex box would work with both new and old browsers,
                    e.g., android 4.1, android 4.3, iOS Safari, Safari, IE 10.
                    Import vr_flex and you`re ready to go!}
  # A summary of your Compass extension. Should be different than Description
  s.summary = %q{Use Flexbox in all browsers!}
  # The names of the author(s) of the extension.
  # If more than one author, comma separate inside of the brackets
  s.authors = ["Tadas Masidlauskas"]
  # The email address(es) of the author(s)
  # If more than one author, comma separate inside of the brackets
  s.email = ["meshkis16@gmail.com"]
  # URL of the extension
  s.homepage = "https://bitbucket.org/meshkis16/vr_flex"

  # Gem Files
  # These are the files to be included in your Compass extension.
  # Uncomment those that you use.

  # README file
  s.files = ["README.md"]

  # CHANGELOG
  s.files += ["CHANGELOG.md"]

  # Library Files
  s.files += Dir.glob("lib/**/*.*")

  # Sass Files
  s.files += Dir.glob("stylesheets/**/*.*")

  # Template Files
  s.files += Dir.glob("templates/**/*.*")

  # Gem Bookkeeping
  # Versions of Ruby and Rubygems you require
  s.required_rubygems_version = ">= 1.3.6"
  s.rubygems_version = %q{1.3.6}

  # Gems Dependencies
  # Gem names and versions that are required for your Compass extension.
  # These are Gem dependencies, not Compass dependencies. Including gems
  #  here will make sure the relevant gem and version are installed on the
  #  user's system when installing your gem.
  s.add_dependency("sass",      [">=3.2.0"])
  s.add_dependency("compass",   [">= 0.12.1"])
end
