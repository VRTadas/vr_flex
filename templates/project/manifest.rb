# Description
description "This extension implements all necessary syntaxes for today's use:
            by one writing we get all syntaxes for successful work of flex-box. 
            It ensures that flex box would work with both new and old browsers,
            e.g., android 4.1, android 4.3, iOS Safari, Safari, IE 10."

# Stylesheet Import
file '_vr_flex-demo.scss', :like => :stylesheet, :media => 'screen, projection'

# Javascript Import
# file 'scripts.js', :like => :javascript, :to => 'scripts.js'

# General File Import
# file 'README.md', :to => "README.md"

# Compass Extension Help
help %Q{
  Open vr_flex-demo.scss for help and examples.
}

# Compass Extension Welcome Message
#  Users will see this when they create a new project using this template.
welcome_message %Q{
  You have successfully installed the vr_flex demo! Open _vr_flex-demo.scss
    for examples.
}