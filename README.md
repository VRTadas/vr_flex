A simple Flexbox Compass extension 
==================================

This extension implements all necessary syntaxes for today's use:
by one writing we get all syntaxes for successful work of flex-box.
It ensures that flex box would work with both new and old browsers,
e.g., android 4.1, android 4.3, iOS Safari, Safari, IE 10.
Import vr_flex and you`re ready to go!

Open [ _vr_flex-demo.scss ] (https://bitbucket.org/meshkis16/vr_flex/src/8991054a30e902f7d15cb5474d4e46efe5928892/templates/project/_vr_flex-demo.scss) for examples of how to use vr_flex.